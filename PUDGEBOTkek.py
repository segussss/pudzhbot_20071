import discord
from discord.ext import commands
#ИМПОРТИРУЕМ БИБЛИОТЕКИ, ЧТОБЫ КОД РАБОТАЛ.

bot = commands.Bot(command_prefix="!", help_commands=None, intents=discord.Intents.all())
#С ПОМОЩЬЮ ЭТОЙ СТРОКИ МЫ ВЫСТАВЛЯЕМ ПРЕФИКСЫ, УДАЛЯЕМ СТАНДАРТНУЮ КОМАНДУ HELP, ВШИТУЮ В БОТА, А ТАК ЖЕ ИНТЕНТЫ ВРУБАЕМ. (ЧЕ ТАКОЕ ИНТЕНТЫ Я НЕ ЗНАЮ)

CENSORED_WORDS = ['папа', 'мат', 'дурак', 'попа']
# Цензурные слова.

@bot.event
async def on_message(message):
    #функция удаления цензурных слов
    await bot.process_commands(message)
    #мне дальше лень описывать
    for content in message.content.split():
        for censored_word in CENSORED_WORDS:
            if content.lower() == censored_word:
                await message.delete()
                #бот удаляет цензурные слова
                await message.channel.send(f"**{message.author.mention} это запрещено.**")
                #бот пишет что эти слова запрещенные.
#с помощью этой команды удаляются цензурные слова, написанные ранее на 8 строке.

@bot.command()
@commands.has_permissions(kick_members=True, administrator=True)
#выставляем права, которые не дают например использовать команду тем, у кого нет прав администратора.
async def kick(ctx, member: discord.Member, *, reason="Нарушение правил."):
    # функция кика
    await ctx.send(f"**Администратор {ctx.author.mention} кикнул к чертовому отцу)) чилловека {member.mention}**")
    #бот пишет что человек выгнан из сервера.
    await member.kick(reason=reason)
    #бот кикает человека.
#команда, которая выгоняет из сервера человека (тоесть кикает, лол)

@bot.command()
@commands.has_permissions(ban_members=True, administrator=True)
#выставляем права, которые не дают например использовать команду тем, у кого нет прав администратора.
async def ban(ctx, member: discord.Member, *, reason="кееек"):
    #функция бана
    await ctx.send(f"**Администратор {ctx.author.mention} забанил к чертовому отцу)) чилловека {member.mention}**")
    #бот пишет что человек исключен из сервера.
    await member.ban(reason=reason)
    #бот банит человека.
#команда, которая исключает из сервера человека (тоесть банит, лол)

@bot.command()
@commands.has_permissions(administrator = True)
#выставляем права, которые не дают например использовать команду тем, у кого нет прав администратора.
async def mute(ctx, member: discord.Member, *, reason=None):
    #функция мута
    role = discord.utils.get(ctx.guild.roles, id=1108378139635220540)
    #строка, которая позволяет в дальнейшем выдавать роль мута
    await member.add_roles(role, reason=reason)
    #функция мута
    message: discord.Message = await ctx.send(f"**User {member.mention} has been muted by reason: {reason}!**")
    #бот пишет, что человек замучен
    await message.add_reaction("✔️")
    #бот ставит реакцию галочки тип чтобы это))

@bot.command()
@commands.has_permissions(administrator = True)
#выставляем права, которые не дают например использовать команду тем, у кого нет прав администратора.
async def unmute(ctx, member: discord.Member):
    #функция размута
    role = discord.utils.get(ctx.guild.roles, id=1108378139635220540)
    #строка, которая позволяет в дальнейшем выдавать роль размута
    await member.remove_roles(role)
    #функция размута
    message: discord.Message = await ctx.send(f"{member.mention} **успешно размучен**")
    #бот пишет, что человек размучен
    await message.add_reaction("✔️")
    #бот ставит реакцию галочки тип чтобы это))

@bot.command()
@commands.has_permissions(administrator = True)
#выставляем права, которые не дают например использовать команду тем, у кого нет прав администратора.
async def clear(ctx, amount=None):
    #функция удаления сообщений
    await ctx.channel.purge(limit=int(amount))
    #команда, удаляющая сообщений в любом количестве.
    await ctx.channel.send('Сообщения успешно удалены!')
    #бот пишет, что бот удалил сообщения.

bot.run("MTEwMzI5NTU1ODY0ODU5ODYxOA.Gnivro.bK7aNDNJGIOyGhcoF9Fh-kyNMdvQNKKcCRNPXk")
#ТОКЕН БОТА, с помощью которого бот включается и выполняет свои функции.
